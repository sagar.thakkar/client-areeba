package com.example.workflow2.tutorial;

import lombok.extern.slf4j.Slf4j;
import org.camunda.bpm.client.ExternalTaskClient;
import org.camunda.bpm.client.task.ExternalTask;
import org.camunda.bpm.engine.FormService;
import org.camunda.bpm.engine.ProcessEngine;
import org.camunda.bpm.engine.ProcessEngines;
import org.camunda.bpm.engine.TaskService;
import org.camunda.bpm.engine.externaltask.LockedExternalTask;
import org.camunda.bpm.engine.impl.cfg.AbstractProcessEnginePlugin;
import org.camunda.bpm.engine.impl.cfg.ProcessEngineConfigurationImpl;
import org.camunda.bpm.engine.impl.cfg.ProcessEnginePlugin;
import org.camunda.bpm.engine.task.Task;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Slf4j
public class ApplicationWorker {


    public static void main(String[] args) {

        ExternalTaskClient client = ExternalTaskClient.create()
                .baseUrl("http://localhost:8080/rest")
                .asyncResponseTimeout(10000) // long polling timeout
                .build();

        client.subscribe("CreatApp").lockDuration(10000).handler((externalTask, externalTaskService) -> {
            boolean selectData = Math.random() >= 0.5;
            Map<String, Object> variables = new HashMap<>();
            if (selectData) {
                variables.put("data", "Need Leave For fun");
            }
            if (!selectData) {
                variables.put("data", "Need Leave for Heal issues");
            }
            String application = externalTask.getVariable("application");
            System.out.println(application);
            System.out.println("CheckApplication");
            externalTaskService.complete(externalTask, variables);
        }).open();


       client.subscribe("CheckApplication").lockDuration(10000).handler((externalTask, externalTaskService) -> {
           System.out.println(externalTask.getVariable("application").toString());
           String application = externalTask.getVariable("application").toString();
           Map<String, Object> variables = new HashMap<>();
           if (application.equals("approved")) {
               boolean north = true;
               variables.put("north", north);
               System.out.println(north);
               variables.put("status", north);
               System.out.println("=============================================================Approved====================================================================================");
           }
           if (application.equals("rejected")) {
               boolean north = false;
               variables.put("north", north);
               System.out.println(north);
               variables.put("status", north);
               System.out.println("=============================================================Rejected====================================================================================");
           }
           externalTaskService.complete(externalTask, variables);
       }).open();


    }

}
