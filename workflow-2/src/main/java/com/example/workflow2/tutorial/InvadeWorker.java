package com.example.workflow2.tutorial;

import lombok.extern.slf4j.Slf4j;
import org.camunda.bpm.client.ExternalTaskClient;
import org.camunda.bpm.engine.delegate.DelegateExecution;
import org.camunda.bpm.engine.delegate.JavaDelegate;
import org.camunda.bpm.engine.variable.Variables;

import java.awt.*;
import java.net.URI;
import java.util.HashMap;
import java.util.Map;

@Slf4j
public class InvadeWorker {
    public static void main(String[] args) {
        ExternalTaskClient client = ExternalTaskClient.create()
                .baseUrl("http://localhost:8080/rest")
                .asyncResponseTimeout(10000) // long polling timeout
                .build();

        // subscribe to an external task topic as specified in the process
        client.subscribe("DecideOnExpansion")
                .lockDuration(1000) // the default lock duration is 20 seconds, but you can override this
                .handler(
                (
                        externalTask,
                        externalTaskService
                ) ->
                {
                    // Put your business logic here

                    boolean north = Math.random() >= 0.5;
                    Map<String, Object> variables = new HashMap<>();
                    variables.put("north", north);


                    // Complete the task                    // Complete the task
                    externalTaskService.complete(externalTask,variables);
                })
                .open();

        client.subscribe("InvadeGaul")
                .lockDuration(1000)
                .handler((externalTask, externalTaskService) -> {
                    log.error("InvadeGaul");
                    externalTaskService.complete(externalTask);
                }).open();

        client.subscribe("InvadePersia")
                .lockDuration(1000)
                .handler((externalTask, externalTaskService) -> {
                    log.error("InvadePersia");
                    externalTaskService.complete(externalTask);
                }).open();

    }
}
