package com.example.workflow2;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Workflow2Application {

    public static void main(String[] args) {
        SpringApplication.run(Workflow2Application.class, args);
    }

}
